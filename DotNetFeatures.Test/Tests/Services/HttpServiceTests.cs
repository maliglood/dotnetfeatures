﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DotNetFeatures.Services;
using Newtonsoft.Json;
using NUnit.Framework;

namespace DotNetFeatures.Test.Tests.Services
{
    public class HttpServiceTests
    {
        // [Ignore("It's not for autotests")]
        [TestCase("50:50:20501:37", "1", "5", 10, 0, 2)]
        public async Task Request_Test(string id, string from, string to, int limit, int skip, int tolerance)
        {
            const string ROSREESTR_URI = "https://pkk5.rosreestr.ru";

            var httpService = new HttpService();
            var link = $"{ROSREESTR_URI}/api/features/{from}";

            var @params = new Dictionary<string, string>
            {
                {"sqo", id},
                {"sqot", to},
                {"limit", limit.ToString()},
                {"skip", skip.ToString()},
                {"tolerance", tolerance.ToString()}
            };

            var headers = new Dictionary<string, string>
            {
                {"referer", ROSREESTR_URI}
            };

            var response = await httpService.Request(link, HttpMethod.Get, @params, headers);

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            Assert.IsNotNull(response.Content);

            /*var features =
                JsonConvert.DeserializeObject<FeaturesModel>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));*/
        }
    }
}
