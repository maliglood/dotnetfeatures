﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace DotNetFeatures.Configuration
{
    /// <summary>
    /// Helper for work with configuration of system.
    /// </summary>
    public static class ConfigHelper
    {
        /// <summary>
        /// Environment variable's name, default for Visual Studio.
        /// </summary>
        public const string DEFAULT_ENVIRONMENT_VARIABLE_NAME = "ASPNETCORE_ENVIRONMENT";

        /// <summary>
        /// Default name of project settings json-file (without extension).
        /// </summary>
        public const string DEFAULT_CONFIG_FILE_NAME = "appsettings";

        /// <summary>
        /// Environment's name for release.
        /// </summary>
        public const string PRODUCTION_ENVIRONMENT = "Production";

        /// <summary>
        /// Environment's name for debug.
        /// </summary>
        public const string DEVELOPMENT_ENVIRONMENT = "Development";

        /// <summary>
        /// Current environment variable's name.
        /// </summary>
        private static string _environmentVariableName = DEFAULT_ENVIRONMENT_VARIABLE_NAME;

        /// <summary>
        /// Name of project settings json-file.
        /// </summary>
        private static string _configFileName = DEFAULT_CONFIG_FILE_NAME;

        /// <summary>
        /// Current environment's name.
        /// </summary>
        private static string _environmentName = null;

        /// <summary>
        /// Builded configuration.
        /// </summary>
        private static IConfigurationRoot _configuration;

        /// <summary>
        /// Current environment variable's name.
        /// </summary>
        public static string EnvironmentVariableName => _environmentVariableName;

        /// <summary>
        /// Sets new value for environment variable's name.
        /// </summary>
        /// <param name="newValue"> New variable's name. </param>
        public static void SetEnvironmentVariableName(string newValue)
        {
            _environmentVariableName = newValue;
        }

        /// <summary>
        /// Sets new value for name of project settings json-file.
        /// </summary>
        /// <param name="newValue"> New file's name (without extension). </param>
        public static void SetConfigFileName(string newValue)
        {
            _configFileName = newValue;
        }

        /// <summary>
        /// Returns environment's name.
        /// </summary>
        /// <returns></returns>
        public static string GetEnvironment()
        {
            return _environmentName ?? (_environmentName = Environment.GetEnvironmentVariable(EnvironmentVariableName) ?? PRODUCTION_ENVIRONMENT);
        }

        /// <summary>
        /// Returns configuration of system.
        /// </summary>
        /// <returns> Builded Configuration. </returns>
        public static IConfigurationRoot GetConfiguration()
        {
            return _configuration ?? (_configuration = new ConfigurationBuilder()
                .AddJsonFile($"{_configFileName}.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"{_configFileName}.{GetEnvironment()}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build());
        }
    }
}
