﻿namespace DotNetFeatures.Enums
{
    public enum WebMethods
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
