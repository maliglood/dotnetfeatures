﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using DotNetFeatures.Enums;
using Newtonsoft.Json;

namespace DotNetFeatures.Services
{
    /// <summary>
    /// Service for http requests.
    /// </summary>
    public class HttpService
    {
        /// <summary>
        /// Default timeout (in seconds).
        /// </summary>
        private const int DEFAULT_TIMEOUT = 25;

        /// <summary>
        /// Request waiting timeout.
        /// </summary>
        private readonly int _timeout;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeout"> Request waiting timeout. </param>
        public HttpService(int timeout = DEFAULT_TIMEOUT)
        {
            _timeout = timeout;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"> Uri for request. </param>
        /// <param name="httpMethod"> Type of request. </param>
        /// <param name="body"> Reauest's body. </param>
        /// <param name="headers"> Reauest's headers. </param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> Request(
            string uri
            , HttpMethod httpMethod
            , IEnumerable<KeyValuePair<string, string>> body = null
            , IEnumerable<KeyValuePair<string, string>> headers = null
        )
        {
            try
            {
                using (var client = new HttpClient { Timeout = TimeSpan.FromSeconds(_timeout) })
                {
                    if (headers != null)
                    {
                        foreach (var header in headers)
                        {
                            client.DefaultRequestHeaders.Add(header.Key, header.Value);
                        }
                    }

                    HttpRequestMessage httpRequestMessage;

                    if (httpMethod == HttpMethod.Get)
                    {
                        var @params = string.Empty;
                        var separator = string.Empty;

                        if (body != null)
                        {
                            @params = string.Join("&", body.Select(x => $"{x.Key}={x.Value}"));
                            separator = "?";
                        }

                        httpRequestMessage = new HttpRequestMessage
                        {
                            Method = httpMethod,
                            RequestUri = new Uri($"{uri}{separator}{@params}"),
                        };
                    }
                    else
                    {
                        httpRequestMessage = new HttpRequestMessage
                        {
                            Method = httpMethod,
                            RequestUri = new Uri(uri),
                            Headers =
                            {
                                { "Content-Type", "application/json" }
                            },
                            Content = new StringContent(JsonConvert.SerializeObject(body))
                        };
                    }

                    return await client.SendAsync(httpRequestMessage);
                }
            }
            catch (HttpRequestException)
            {
                return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
            }
            catch (TaskCanceledException)
            {
                return new HttpResponseMessage(HttpStatusCode.RequestTimeout);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.RequestTimeout);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"> Uri for request. </param>
        /// <param name="webMethod">  </param>
        /// <param name="body"> Reauest's body. </param>
        /// <param name="headers"> Reauest's headers. </param>
        /// <returns></returns>
        [Obsolete("Will be deprecated. Use HttpMethod instead WebMethods in parameters.")]
        public async Task<HttpResponseMessage> Request(
            string uri
            , WebMethods webMethod
            , IEnumerable<KeyValuePair<string, string>> body = null
            , IEnumerable<KeyValuePair<string, string>> headers = null
        )
        {
            HttpMethod httMethod;

            switch (webMethod)
            {
                case WebMethods.GET:
                    httMethod = HttpMethod.Get;
                    break;
                case WebMethods.POST:
                    httMethod = HttpMethod.Post;
                    break;
                case WebMethods.PUT:
                    httMethod = HttpMethod.Put;
                    break;
                case WebMethods.DELETE:
                    httMethod = HttpMethod.Delete;
                    break;
                default:
                    throw new Exception("Unknown web-method");
            }

            return await Request(uri, httMethod, body, headers);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TParams"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="uri"></param>
        /// <param name="httpMethod"> Default is GET. </param>
        /// <param name="body"></param>
        /// <param name="headers"> Additional headers for request. </param>
        /// <returns></returns>
        public async Task<TResponse> Complete<TParams, TResponse>(
            string uri
            , HttpMethod httpMethod = null
            , TParams body = null
            , IEnumerable<KeyValuePair<string, string>> headers = null
        )
        where TParams : class
        {
            if (httpMethod == null)
            {
                httpMethod = HttpMethod.Get;
            }

            Dictionary<string, string> bodyProperties;

            if (body != null)
            {
                if (body is Dictionary<string, string> properties)
                {
                    bodyProperties = properties;
                }
                else
                {
                    bodyProperties = body.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .ToDictionary(prop => prop.Name, prop => prop.GetValue(body, null).ToString());
                }
            }
            else
            {
                bodyProperties = new Dictionary<string, string>();
            }

            var responseMessage = await Request(uri, httpMethod, bodyProperties, headers);

            if (responseMessage.StatusCode == HttpStatusCode.OK && responseMessage.Content != null)
            {
                try
                {
                    var response = JsonConvert.DeserializeObject<TResponse>(await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                throw new Exception($"Response has status: {responseMessage.StatusCode} and content: {responseMessage.Content}");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TParams"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="uri"></param>
        /// <param name="webMethod"></param>
        /// <param name="body"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        [Obsolete("Will be deprecated. Use HttpMethod instead WebMethods in parameters.")]
        public async Task<TResponse> Complete<TParams, TResponse>(
            string uri
            , WebMethods webMethod = WebMethods.GET
            , TParams body = null
            , IEnumerable<KeyValuePair<string, string>> headers = null
        )
            where TParams : class
        {
            HttpMethod httMethod;

            switch (webMethod)
            {
                case WebMethods.GET:
                    httMethod = HttpMethod.Get;
                    break;
                case WebMethods.POST:
                    httMethod = HttpMethod.Post;
                    break;
                case WebMethods.PUT:
                    httMethod = HttpMethod.Put;
                    break;
                case WebMethods.DELETE:
                    httMethod = HttpMethod.Delete;
                    break;
                default:
                    throw new Exception("Unknown web-method");
            }

            return await Complete<TParams, TResponse>(uri, httMethod, body, headers);
        }
    }
}
